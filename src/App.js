import React, { Component } from 'react';
import {Toggler, TogglerItem} from './toggler';
import CompWithPropTypes from './compWithPropTypes';

import './App.css';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      data: {
      	name: "",
      	surname: "",
        action: "",
        activeState: "",
        children: "",
        activeToggler: "left",
      }
    };
  }
//event

handleSubmit = (event) => {
   event.preventDefault();
   console.log('submitted data',this.state.data);
 }

handleFormChange = event => {
   let value = event.target.value;
   let name = event.target.name;
   console.log(name, value);

   this.setState({
     data:{
       ...this.state.data,
       [name]: value
     }});
 }
						  changeStatus = (event) => {
						    let TogglerValue = event.target.innerText;
						    this.setState({
						      data:{
						        ...this.state.data,
						        activeToggler: TogglerValue
						      }});
						  }

  render() {
  						  let {activeToggler} = this.state.data;
    return (
      <div className="App">

      <form onSubmit={this.handleSubmit} autoComplete="off">
          <label>
            <div>Name</div>
            <input
              type='text'
              name='name'
              onChange={this.handleFormChange}
            />
          </label>
          <label>
            <div>Surname</div>
            <input
              type='text'
              name='surname'
              onChange={this.handleFormChange}
            />
          </label>
          <br/>
									<Toggler
									  name="Choose layout"
									  activeToggler={activeToggler}
									  changeStatus={this.changeStatus}
									>
									    <TogglerItem name="left"/>
									    <TogglerItem name="center"/>
									    <TogglerItem name="right"/>
									</Toggler>
            <button type="submit">Send</button>
          </form>

      </div>
    );
  }
}

export default App;
